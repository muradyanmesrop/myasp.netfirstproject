﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace HomeWork_Stage4_Lesson3.Controllers
{
    [ApiController]
    [Route("[controller]")]
    public class CarsController : ControllerBase
    {
        private readonly ILogger<CarsController> _logger;
        public CarsController(ILogger<CarsController> logger)
        {
            _logger = logger;
        }

        private static List<string> Colores = new List<string>()
        {
            "Red", "Black", "Wite", "Blue", "Brown", "Orange"
        };

        private static List<string> Makes = new List<string>()
        {
            "BMW", "Bugatti", "Mercedes-Benz", " Mitsubishi", "Ford", " Rolls-Royce"
        };

        [HttpGet]
        public IEnumerable<Car> Get()
        {
            Random rnd = new Random();
            
            for (int i = 0, count = Makes.Count; i < count; i++)
            {
                int index = rnd.Next(0, count);
                yield return new Car
                {
                    Make = Makes[i],
                    PriceUSD = rnd.Next(50000, 100000),
                    Color = Colores[index]
                };
            }
        }

    }
}
