﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace HomeWork_Stage4_Lesson3
{
    public class Car
    {
        public string Make { get; set; }
        public decimal PriceUSD { get; set; }
        public decimal PriceAMD => PriceUSD * 522;
        public string Color { get; set; }
    }
}
